# Backend-API Assignment

I have created the API as it was explained in the document.  
I tried to create the best possible API architecture and kept the project modular. I also used the best coding practices using features of NodeJS upto full extent.

## How to use the API with Postman?

The API can be accessed with this URL : [https://smallcase-api-st.herokuapp.com](http://smallcase-api-st.herokuapp.com)

API Documentation is given here : [https://documenter.getpostman.com/view/3481116/Rztispqv](https://documenter.getpostman.com/view/3481116/RztitAKq)

To test the API calls you can directly import the environment in postman.  
Steps to add the Postman config is:

1. Click the above link or [here](https://documenter.getpostman.com/view/3481116/RztitAKq). A page with the API documentation will open.  
   Postman config file can also be downloaded from [here](./Smallcase-API.postman_collection.json) manually.
1. Click the `Run in Postman` button present in the top-right corner.
1. All the endpoints will be loaded in local system.
1. Examples are also given in the documentation

## Tech Stack

1. **NodeJS** - JavaScript engine for running back end JS code
1. **ExpressJS** - Web Framework used to create API
1. **mongoDB** - no-SQL database to store data
1. **mongoose** - ORM for mongoDB
1. **Postman** - For writing and publishing API documentation
1. **Docker** - For containerization of the application
1. **Heroku** - For hosting Docker Image in the Heroku Container Registry

## Directory Strucutre

```bash
│
├─ models
│   ├── config
│   │    └─ index.js
│   │
│   ├─ Holdings.js
│   ├─ Stocks.js
│   ├─ Trades.js
│   └─ Users.js
│
├─ routes
│   ├─ api
│   │   ├─ holdings.js
│   │   ├─ index.js
│   │   ├─ portfolio.js
│   │   ├─ returns.js
│   │   └─ trade.js
│   │
│   └─ index.js
│
├─ utils
│   ├─ dbFunctions.js
│   ├─ formulas.js
│   └─ log.js
│
├─ .env
├─ .gitignore
├─ .dockerignore
├─ .eslintrc.json
├─ node_modules
├─ Dockerfile
├─ server.js
├─ package.json
├─ package-lock.json
└─ README.md
```

- The directory structure is kept as such so that **future scalability** of the project will be painless.
- All the new routes can be added under the routes directory which can be further sub divided.
  - For example in the current structure all the sub routes coming under `api` route are kept inside the `api` directory.
  - All the routes can be managed locally to the sub directory by changing the `./routes/api/index.js` file.
- Similarly models can also be added and configured in the similar manner.

## Database

- `MongoDB` is used for storage of data.
- In **development** environment local mongoDB server is used for faster development running on local machine port `27017`.
- In **production** environment _mLab_'s free tier db is used.

## Models

- `mongoose` is used in the project which is an ORM used with MongoDB. It helps in creating all the schemas in a robust and rigid manner. It also provides simpler querying tools.  
  All the models are managed with mongoose.
- If seen in detail, I have declared the Schema structure which takes full advantage of validation provided by mongoose.
- For example:

  ```javascript
  const UserSchema = new mongoose.Schema({
    candidateName: {
      type: String,
      required: true,
      trim: true
    },
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      validate: {
        validator: validator.isEmail,
        message: "{VALUE} is not a valid email"
      }
    }
  });
  ```

- Here we can see that UserSchema is defined which **validates** the input email address with help of `validator` module. It also checks whether it is unique or not.
- Also various other mongoose features such as static functions and methods are used for querying database.

## Production environment setup

- The API is deployed on **Heroku**. It doesn't uses Heroku's normal application builds, rather I have used **Heroku Container Registry** where I have deployed my custom container.
- **Docker** is used for containerization. I have used custom `Dockerfile` which uses latest image of node to run the application.
- For production database `mLab`'s free tier service is used.
- For high scalability container images can be deployed directly with **Kubernetes**.

## Environment variables

- `.env` file is used to set environment variables. `dotenv` module is used which includes all the env variables to the root of the application.

- `.env` example is given in the [example.env](./example.env) file

## Development tools

- **VS-Code** - All the development was done in Visual Studio Code
- **git** - git is used for version control
- **eslint** - It helps in keeping the code semantic and unifies the coding style all over the project.
- **Postman** - Used to test the API and also for writing API documentation.

## Possible improvements

1. Rate limitting can be introduced which will make the API more secure.
1. Auth middleware can be used to authenticate and authorize the requests.
1. API versions can be added which can be easily done(because of scalable directory structure) if needed in future.
1. More endpoints can be added which can return a more specific response with specific data.
1. Testing can be added to check the API response. Supertest can be used with mocha.
1. Continous integration (like Jenkins) can be used after writing tests. This will be helpful in continous deployment.
1. **Kuberntetes** can be used to deploy containers.

## Design decisions

1. **Decision:** `.env` file is used to store all the environment variables.  
   **Reason:** This helps as environment variables can be stored in a single place and can be used in any file if required.

1. **Decision:** Mongoose static functions are made and used as can be seen in some js files:

   - [Holdings.js](./models/Holdings.js#L36)
   - [Users.js](./models/Users.js#L28)

   **Reason:** This increases the readability of the code and increases semantics i.e. the function names tell itself what it is doing.

1. **Decision:** Mongoose pre save middleware is used when saving a Trade transaction which is done in [Trades.js](./models/Trades.js#L103)  
   **Reason:** This is used so that before saving every transaction the portfolio as well as the stocks collections are updated automatically, this reduces the code which you have to write each time when doing a trade (i.e. either sell or buy).

1. **Decision:** Custom options are added in Schemas to perform some actions in files:

   - [Trades.js](./models/Trades.js#L48)
   - [Users.js getId()](./models/Users.js#L40)
   - [Users.js toJSON()](./models/Users.js#L48)

   **Reason:** These functions simply returns the desired data each time and we don't need to perform them in each route manually.

1. **Decision:** [dbFunctions.js](./utils/dbFunctions.js) file created and addition and deletion of trade functions moved in that utils file.  
   **Reason:** Due to usage of those functions in repetitve manner, it was important to make those function modulars.
