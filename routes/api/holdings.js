const router = require('express').Router();
const mongoose = require('mongoose');
const log = require('../../utils/log');

const Users = mongoose.model('users');
const Holdings = mongoose.model('holdings');

// returning all the holdings of a particular user
router.get('/', async (req, res) => {
  const { email } = req.query;
  if (email === undefined) {
    res.status(400).send('Email id not provided as url query');
  } else {
    try {
      const user = await Users.findByEmail(email);
      // eslint-disable-next-line no-underscore-dangle
      const holdings = await Holdings.findByUserId(user._id);
      res.send({ ...user.toJSON(), holdings: holdings.holdings });
    } catch (error) {
      log.Error(error);
      res.status(400).send({ error: error.message });
    }
  }
});

module.exports = router;
