const router = require('express').Router();
const mongoose = require('mongoose');
// const log = require('../../utils/log');

// importing schemas
const Users = mongoose.model('users');
const Trades = mongoose.model('trades');

router.get('/:tickerSymbol?', async (req, res) => {
  const { email } = req.query;
  const { tickerSymbol } = req.params;

  if (email === undefined) {
    res.status(400).send('Email id not provided in url query');
  } else {
    let user;
    try {
      // finds the user with self made findByEmail method
      user = await Users.findByEmail(email);
    } catch (error) {
      res.status(400).send({ error: error.message });
      return;
    }
    let find = {};

    // checks if the user is asking for a specific stock portfolio
    // if yes then trades only of that particular stock is sent
    // else all the stocks are compiled and sent
    if (tickerSymbol !== undefined) find = { tickerSymbol };
    // eslint-disable-next-line no-underscore-dangle
    const userTrades = await Trades.find({ userId: user._id, ...find });

    // creating custom object to send as the response to the client
    const tickerTrades = userTrades.reduce((accumulator, current) => {
      if (accumulator[current.tickerSymbol] === undefined) {
        accumulator[current.tickerSymbol] = [];
      }
      accumulator[current.tickerSymbol].push(current);
      return accumulator;
    }, {});

    // sending email id along with the trades
    res.send({ user_email: user.email, trades: tickerTrades });
  }
});

module.exports = router;
