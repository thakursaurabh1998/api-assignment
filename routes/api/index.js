const router = require('express').Router();

// routes for all the controllers coming after /api
router.use('/trade', require('./trade'));
router.use('/portfolio', require('./portfolio'));
router.use('/holdings', require('./holdings'));
router.use('/returns', require('./returns'));

module.exports = router;
