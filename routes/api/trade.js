const router = require('express').Router();
const log = require('../../utils/log');

// using dbFunctions to perform addition and deletion of trades
const { addTrade, deleteTrade } = require('../../utils/dbFunctions');

// takes in action as the parameter in route
router.post('/:action', (req, res) => {
  // extracting the necessary variables from body
  const { tickerSymbol, email, stockQuantity } = req.body;
  const { action } = req.params;

  // saving trade and sending the response back
  addTrade(email, stockQuantity, action, tickerSymbol)
    .then((data) => {
      res.status(201).send(data);
    })
    .catch((err) => {
      log.Error(err);
      // respond 400 with any error message
      res.status(400).send({ error: err.message });
    });
});

router.delete('/:transactionId', (req, res) => {
  // extracting the necessary variables from body
  const { transactionId } = req.params;
  deleteTrade(transactionId)
    .then(() => {
      res.status(200).send({ delete: 'successful' });
    })
    .catch((error) => {
      // respond 400 with any error message
      res.status(400).send({ error: error.message });
    });
});

router.patch('/:transactionId', (req, res) => {
  // extracting the necessary variables from body
  const {
    action, tickerSymbol, email, stockQuantity,
  } = req.body;
  const { transactionId } = req.params;
  deleteTrade(transactionId)
    .then(() => {
      addTrade(email, stockQuantity, action, tickerSymbol)
        .then(() => {
          res.send({ updated: true });
        })
        .catch(err => res.status(400).send({ error: err.message }));
      // respond 400 with any error message
    })
    .catch(err => res.status(400).send({ error: err.message }));
});

module.exports = router;
