const router = require('express').Router();
const mongoose = require('mongoose');
// const log = require('../../utils/log');
const { cumulativeReturns } = require('../../utils/formulas');

// importing schemas
const Users = mongoose.model('users');
const Stocks = mongoose.model('stocks');
const Holdings = mongoose.model('holdings');

router.get('/', async (req, res) => {
  const { email } = req.query;
  if (email === undefined) {
    res.status(400).send('Email id not provided in url query');
  } else {
    // getting current user portfolio and current stocks data
    const user = await Users.findByEmail(email);
    const stocks = await Stocks.find();

    // eslint-disable-next-line no-underscore-dangle
    const holdings = await Holdings.findByUserId(user._id);

    // cumulativeReturns function to calculate the totalReturns using above data
    const totalReturns = cumulativeReturns(stocks, holdings.holdings);
    res.send({ email, totalReturns });
  }
});

module.exports = router;
