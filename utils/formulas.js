/* eslint-disable max-len */

// function to calculate weighted average
const weightedAvg = (oldPrice, newPrice, oldQuantity, newQuantity) => (newPrice * newQuantity + oldPrice * oldQuantity) / (newQuantity + oldQuantity);

const reverseWeightedAvg = (currentAvgPrice, stockPrice, currentQuantity, stockQuantity) => {
  if (currentQuantity === stockQuantity) return 0;
  return (
    (currentAvgPrice * currentQuantity - stockPrice * stockQuantity)
    / (currentQuantity - stockQuantity)
  );
};

/*
  function to calculate cumulative returns
  @param: stocks array with current stocks data
  @param: holdings array of the user
*/
const cumulativeReturns = (stocks, holdings) => {
  const profileStockData = {};

  // using reduce to create an object which can be used easily to calculate returns
  holdings.reduce((accumulator, current) => {
    accumulator[current.tickerSymbol] = {
      currentShares: current.shares,
      avgBuyPrice: current.avgBuyPrice,
    };
    return accumulator;
  }, profileStockData);

  stocks.reduce((accumulator, current) => {
    if (accumulator[current.tickerSymbol] !== undefined) {
      accumulator[current.tickerSymbol].currentPrice = current.currentPrice;
    }
    return accumulator;
  }, profileStockData);

  let totalReturns = 0;
  Object.keys(profileStockData).forEach((ticker) => {
    const currentStock = profileStockData[ticker];
    totalReturns
      += (currentStock.currentPrice - currentStock.avgBuyPrice) * currentStock.currentShares;
  });

  return parseFloat(totalReturns.toFixed(3));
};

module.exports = {
  cumulativeReturns,
  reverseWeightedAvg,
  weightedAvg,
};
