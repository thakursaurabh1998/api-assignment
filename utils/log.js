/* eslint-disable no-console */
const chalk = require('chalk');

// Used to print log data in custom colors for
// better readability of errors and info
const Log = type => (text) => {
  console.log(type(text));
  if (process.env.NODE_ENV === 'development' && typeof text === 'object') {
    console.log(chalk.red('LOGGER ERROR: '));
    console.log(text);
  }
};

module.exports = {
  Print: Log(chalk.white),
  Error: Log(chalk.red),
  Info: Log(chalk.green),
  Warning: Log(chalk.yellow),
  Debug: Log(chalk.cyan),
  Logger: Log(chalk.magenta.underline),
};
