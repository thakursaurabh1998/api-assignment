const mongoose = require('mongoose');
const { reverseWeightedAvg } = require('../utils/formulas');

// importing schemas
const Trades = mongoose.model('trades');
const Users = mongoose.model('users');
const Holdings = mongoose.model('holdings');
const Stocks = mongoose.model('stocks');

const addTrade = async (email, stockQuantity, action, tickerSymbol) => {
  let user;
  try {
    user = await Users.findByEmail(email);
  } catch (error) {
    throw new Error(error.message);
  }
  const trade = new Trades({
    tickerSymbol,
    stockQuantity,
    action,
    userId: user.getId(),
  });

  // saving trade and sending the promise back
  return trade.save();
};

const deleteTrade = async (transactionId) => {
  const trade = await Trades.findById(transactionId);
  if (trade === null) {
    throw new Error('Trade with transaction id not found');
  }
  if (trade.action === 'sell') {
    await Holdings.update(
      { userId: trade.userId, 'holdings.tickerSymbol': trade.tickerSymbol },
      {
        $inc: {
          'holdings.$.shares': trade.stockQuantity,
        },
      },
    );
  } else if (trade.action === 'buy') {
    const holdings = await Holdings.findByUserId(trade.userId);
    const currentHolding = holdings.holdings.find(e => e.tickerSymbol === trade.tickerSymbol);
    if (currentHolding.shares < trade.stockQuantity) {
      throw new Error('inadequate shares in account');
    }
    await Holdings.update(
      { userId: trade.userId, 'holdings.tickerSymbol': trade.tickerSymbol },
      {
        $inc: {
          'holdings.$.shares': -trade.stockQuantity,
        },
        $set: {
          'holdings.$.avgBuyPrice': reverseWeightedAvg(
            currentHolding.avgBuyPrice,
            trade.buyingPrice,
            currentHolding.shares,
            trade.stockQuantity,
          ),
        },
      },
      { runValidators: true },
    );
  }
  await Stocks.update(
    { tickerSymbol: trade.tickerSymbol },
    {
      $inc: {
        quantity: -trade.stockQuantity,
      },
    },
  );
  await Trades.findOneAndDelete({ _id: transactionId });
};

module.exports = {
  addTrade,
  deleteTrade,
};
