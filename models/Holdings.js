const mongoose = require('mongoose');

const HoldingsSchema = new mongoose.Schema({
  userId: {
    // reference to the user that is the owner of the holding
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
    unique: true,
    required: true,
  },
  // storing all the holings in arrays with all the
  // necessary data about the stocks in each object
  holdings: [
    {
      tickerSymbol: {
        type: String,
        required: true,
      },
      avgBuyPrice: {
        type: Number,
        default: 0.0,
        required: true,
      },
      shares: {
        type: Number,
        default: 0,
        required: true,
        min: [0, "Shares can't be less than 0"],
      },
    },
  ],
});

// custom query to get holdings of a user for better semantics
// eslint-disable-next-line func-names
HoldingsSchema.statics.findByUserId = function (userId) {
  const Holding = this;
  return Holding.findOne({ userId }).then(
    userHoldings => new Promise((resolve, reject) => {
      if (userHoldings === null) reject(new Error('Holdings of the input userId not found'));
      else resolve(userHoldings);
    }),
  );
};

mongoose.model('holdings', HoldingsSchema);
