const mongoose = require('mongoose');
const path = require('path');
const log = require('../../utils/log');

require('dotenv').config({ path: path.resolve(process.cwd(), '../.env') });

let dbhost;
let dbport;

// used to make the mongoDB URI according to the environment
if (process.env.NODE_ENV === 'production') {
  dbhost = process.env.DB_PRODUCTION_HOST;
  dbport = process.env.DB_PRODUCTION_PORT;
} else {
  dbhost = process.env.DB_HOST;
  dbport = process.env.DB_PORT;
}

const dbuser = process.env.DB_USER;
const dbpassword = process.env.DB_PASSWORD;
const dbname = process.env.DB_NAME;

const mongoURL = `mongodb://${dbuser}:${dbpassword}@${dbhost}:${dbport}/${dbname}`;
mongoose.Promise = global.Promise;

mongoose
  .connect(
    mongoURL,
    { useNewUrlParser: true },
  )
  .catch(err => log.Error(err));

// Listens on successfull connection
mongoose.connection.on('connected', () => log.Info('MongoDB Connected'));

// Listens error
mongoose.connection.on('error', err => log.Error(`error: ${err}`));

// Listens on connection stop
mongoose.connection.on('disconnected', () => log.Info('MongoDB disconnected'));

require('../Stocks');
require('../Holdings');
require('../Users');
require('../Trades');
