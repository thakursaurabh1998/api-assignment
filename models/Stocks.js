const mongoose = require('mongoose');

const StocksSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  tickerSymbol: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  quantity: {
    type: Number,
    required: true,
    min: [0, 'No stocks left'], // minimum value should be 1 else custom error is thrown
  },
  currentPrice: {
    type: Number,
    required: true,
    default: 100.0,
  },
});

mongoose.model('stocks', StocksSchema);
