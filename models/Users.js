/* eslint-disable func-names */
const validator = require('validator');
const mongoose = require('mongoose');
// const log = require('../utils/log');

/*
  User schema in which userName, userId, email are stored
*/
const UserSchema = new mongoose.Schema({
  candidateName: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email',
    },
  },
});

// custom function to find a user by the email address provided directly for better semantics
UserSchema.statics.findByEmail = function (email) {
  const User = this;

  return User.findOne({ email }).then(
    user => new Promise((resolve, reject) => {
      if (user === null) reject(new Error('User email id not found'));
      else resolve(user);
    }),
  );
};

// custom function to get id of the user directly
UserSchema.methods.getId = function () {
  const user = this;
  const { _id } = user.toObject();

  return _id;
};

// overrides the json document which is returned after performing a function
UserSchema.methods.toJSON = function () {
  const user = this;
  const { _id, email, candidateName } = user.toObject();

  return {
    userId: _id,
    email,
    candidateName,
  };
};

mongoose.model('users', UserSchema);
