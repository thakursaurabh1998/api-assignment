/* eslint-disable no-unused-vars */
const mongoose = require('mongoose');
const log = require('../utils/log');
const { weightedAvg } = require('../utils/formulas');

const Stocks = mongoose.model('stocks');
const Holdings = mongoose.model('holdings');

/*
  Trades schema uses USER reference and trades of all the users
  are stored and referenced with the user id
  DB can return all the trades of a single user and also
  trades of a particular stock if filtered with tickerSymbol and userId
*/
const TradesSchema = new mongoose.Schema({
  tickerSymbol: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId, // reference to the user that made the trade
    ref: 'users',
    required: true,
  },
  buyingPrice: {
    type: Number,
  },
  stockQuantity: {
    type: Number,
    required: true,
    min: [1, 'You have to buy atleast 1 stock'], // validation check
  },
  action: {
    // buy or sell
    type: String,
    required: true,
  },
  transactionTime: {
    type: String,
    required: true,
    default: new Date().toString(),
  },
});

// toJSON function over ride to return custom
// JSON as per requirements of the DB object
// eslint-disable-next-line func-names
TradesSchema.methods.toJSON = function () {
  const trade = this;
  const {
    _id, stockQuantity, tickerSymbol, buyingPrice, transactionTime,
  } = trade.toObject();

  return {
    transactionId: _id,
    stockQuantity,
    tickerSymbol,
    buyingPrice,
    transactionTime,
  };
};

// Function to update stocks schema accordingly
const updateStocks = (tickerSymbol, quantity) => Stocks.update(
  { tickerSymbol },
  {
    $inc: {
      quantity,
    },
  },
);

// function to update holdings schema if a share is bought or sell
const updateHoldings = (userId, tickerSymbol, quantity, newAvgBuyPrice) => Holdings.update(
  { userId, 'holdings.tickerSymbol': tickerSymbol },
  {
    $inc: {
      'holdings.$.shares': quantity,
    },
    $set: {
      'holdings.$.avgBuyPrice': newAvgBuyPrice,
    },
  },
);

// function to push a value in holdings array
// in holdings schema if a stock is bought for the first time
const pushHoldings = (userId, tickerSymbol, shares, avgBuyPrice) => Holdings.update(
  { userId },
  {
    $push: {
      holdings: {
        tickerSymbol,
        shares,
        avgBuyPrice,
      },
    },
  },
);

// pre hook on save function to check whether desired stocks are available
// eslint-disable-next-line func-names
TradesSchema.pre('save', async function (next) {
  const trade = this;
  const { tickerSymbol, action } = trade;
  const { stockQuantity } = trade;
  // fetching the current Holdings structure
  const holdings = await Holdings.findOne({
    userId: trade.userId,
    holdings: { $elemMatch: { tickerSymbol } },
  });

  // searching for index with current stock for further repetitive use
  const currentHoldingIndex = holdings === null
    ? -1
    : holdings.holdings.findIndex(element => element.tickerSymbol === tickerSymbol);
  let response = { ok: 0 };

  // collecting current data of required stock
  const liveStock = await Stocks.findOne({ tickerSymbol });
  const buyingPrice = liveStock.currentPrice;

  // setting the buy price to save in the database
  trade.buyingPrice = liveStock.currentPrice;

  if (action === 'buy') {
    if (liveStock.quantity < stockQuantity) {
      throw new Error('Available stocks quantity is less than requested stocks');
    } else if (holdings !== null) {
      // updating the holdings collection if every thing is validated
      response = await updateHoldings(
        trade.userId,
        tickerSymbol,
        stockQuantity,
        weightedAvg(
          holdings.holdings[currentHoldingIndex].avgBuyPrice,
          buyingPrice,
          holdings.holdings[currentHoldingIndex].shares,
          stockQuantity,
        ),
      );
    } else if (holdings === null) {
      // pushing new holdings object if the stock is bought for the first time
      response = await pushHoldings(trade.userId, tickerSymbol, stockQuantity, buyingPrice);
    }
    // if the transaction completes successfully stocks collection is also updated
    if (response.ok === 1) {
      await updateStocks(tickerSymbol, -stockQuantity);
    }
  } else if (action === 'sell') {
    let usershares = 0;
    if (holdings !== null) {
      usershares = holdings.holdings[currentHoldingIndex].shares;
    }
    if (stockQuantity > usershares) {
      throw new Error("User doesn't have required number of shares to sell");
    } else {
      // updating the holdings collection if every thing is validated
      response = await updateHoldings(
        trade.userId,
        tickerSymbol,
        -stockQuantity,
        holdings.holdings[currentHoldingIndex].avgBuyPrice,
      );
    }

    // if the transaction completes successfully stocks collection is also updated
    if (response.ok === 1) {
      await updateStocks(tickerSymbol, stockQuantity);
    }
  }
});

mongoose.model('trades', TradesSchema);
