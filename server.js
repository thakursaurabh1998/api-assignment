require('dotenv').config();
require('./models/config');
const express = require('express');
const bodyParser = require('body-parser');
const log = require('./utils/log');

const app = express();

if (process.env.NODE_ENV === undefined) process.env.NODE_ENV = 'development';

// parse application/json
app.use(bodyParser.json());

// custom logger to log data for debugging purposes
app.use((req, res, next) => {
  const now = new Date().toString().slice(4, 24);
  const reqURL = req.url;
  res.on('finish', () => {
    log.Logger(`${now} ${req.method} ${res.statusCode} ${reqURL}`);
  });
  next();
});

app.use(require('./routes'));

const port = process.env.PORT || 3000;

app.listen(port, () => {
  log.Info(`This is a ${process.env.NODE_ENV} environment.\nServer is up on port ${port}`);
});

module.exports = { app };
